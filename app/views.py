from django.shortcuts import render, redirect
import requests
from bs4 import BeautifulSoup as bs
from .forms import *
import asyncio
import aiohttp


reg = '((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*'
domainsdb_url = 'https://api.domainsdb.info/v1/domains/search?domain='


def parse_web_page(url, request):
    doms = []
    resp = requests.get(url)
    if resp.status_code == 200:
        ses_doms = request.session.get('doms', {})
        if url in ses_doms:
            doms = ses_doms[url]
        else:
            request.session['current_page'] = 1
            text = resp.text
            soup = bs(text, 'html.parser')
            vac_names = soup.find_all('a', href=True)
            vac_names_gen = [parse_url(n["href"]) for n in vac_names if bool(re.match(reg, n['href']))]
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            res, _ = loop.run_until_complete(asyncio.wait(vac_names_gen))
            doms = []
            for r in res:
                doms += r.result()
            ses_doms[url] = doms
            request.session['doms'] = ses_doms
    return doms


async def parse_url(url):
    doms = []
    async with aiohttp.ClientSession() as session:
        async with session.get(f'{domainsdb_url}{url}') as resp:
            try:
                r_json = await resp.json()
                if 'domains' in r_json:
                    for js in r_json['domains']:
                        doms.append({'url': url,
                                     'domain': js['domain'],
                                     'create_date': js['create_date'],
                                     'update_date': js['update_date'],
                                     'country': js['country'],
                                     'is_dead': js['isDead'],
                                     'a': js['A'],
                                     'ns': js['NS'],
                                     'cname': js['CNAME'],
                                     'mx': js['MX'],
                                     'txt': js['TXT']})
            except Exception as e:
                pass
    return doms


def main_page(request):
    filter_choices = ['url', 'domain', 'create_date', 'update_date', 'country', 'is_dead']
    search_choices = ['url', 'domain', 'create_date', 'update_date', 'country', 'is_dead',
                      'a', 'ns', 'cname', 'mx', 'txt']
    filter_field = request.session.get('filter_field', 'url')
    current_url = request.session.get('current_url', '')
    current_page = request.session.get('current_page', 1)
    request.session['current_page'] = current_page
    pages_count = request.session.get('pages_count', 0)
    page_size = request.session.get('page_size', 30)
    doms = request.session.get('doms', {}).get(current_url, [])
    print(filter_field)
    if request.method == 'POST':
        form = UrlForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            current_url = data['url']
            request.session['current_url'] = current_url
            doms = parse_web_page(data['url'], request)
        else:
            errors = form.errors
    search_field = request.session.get('search_field', 'url')
    query = request.session.get('query', '')
    if search_field:
        doms = [d for d in doms if d[search_field] and search_from_obj(d[search_field], query)]
    doms = sorted(doms, key=lambda x: x[filter_field] if x[filter_field] else '')
    pages_count = int(len(doms) // page_size + 1) if len(doms) % page_size != 0 else int(len(doms) / page_size)
    request.session['pages_count'] = pages_count
    doms = doms[(current_page - 1)*page_size:current_page*page_size]
    return render(request, 'main_page.html', locals())


def search_from_obj(object, query, get_it=False):
    if type(object) == list():
        for i in object:
            get_it = search_from_obj(i, query, get_it)
    if type(object) == dict():
        for k, v in object.items():
            get_it = search_from_obj(k, query, get_it)
            get_it = search_from_obj(v, query, get_it)
    else:
        try:
            if query in str(object):
                return True
        except:
            pass
    return get_it


def filter_main_page(request):
    if request.method == 'POST':
        form = FilterForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            field = data['filter_field']
            request.session['filter_field'] = field
        return redirect('main_page')


def search_main_page(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            request.session['current_page'] = 1
            field = data['search_field']
            query = data['query']
            request.session['search_field'] = field
            request.session['query'] = query
        return redirect('main_page')


def change_page_size(request):
    if request.method == 'POST':
        form = ChangePageSize(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            page_size = data['page_size']
            request.session['current_page'] = 1
            request.session['page_size'] = page_size
        return redirect('main_page')


def clean_search(request):
    del request.session['query']
    return redirect('main_page')


def paginate_main_page(request):
    if request.method == 'POST':
        form = PaginateForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            direct = data['direct']
            next_page = 1 if direct == 'next' else -1
            request.session['current_page'] = request.session['current_page'] + next_page
        return redirect('main_page')
