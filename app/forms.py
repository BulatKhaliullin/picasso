from django import forms
import re
from django.core.exceptions import ValidationError


reg = '((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*'


class UrlForm(forms.Form):
    url = forms.CharField()

    def clean_url(self):
        data = self.cleaned_data['url']
        if not bool(re.match(reg, data)):
            raise ValidationError('Please input correct url')
        return data


class FilterForm(forms.Form):
    filter_field = forms.CharField()


class SearchForm(forms.Form):
    search_field = forms.CharField()
    query = forms.CharField()


class PaginateForm(forms.Form):
    direct = forms.CharField()


class ChangePageSize(forms.Form):
    page_size = forms.IntegerField()
