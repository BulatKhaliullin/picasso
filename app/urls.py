from django.urls import path
from .views import *


urlpatterns = [
    path('check_url/', main_page, name='main_page'),
    path('filter/', filter_main_page, name='filter_main_page'),
    path('search/', search_main_page, name='search_main_page'),
    path('clean_search/', clean_search, name='clean_search_main_page'),
    path('paginate/', paginate_main_page, name='paginate_main_page'),
    path('change_page_size/', change_page_size, name='change_page_size_main_page'),
]
